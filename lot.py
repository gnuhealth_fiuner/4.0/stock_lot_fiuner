from trytond.model import fields
from trytond.pool import PoolMeta


class Lot(metaclass=PoolMeta):
    __name__ = 'stock.lot'

    active = fields.Boolean("Active")

    @staticmethod
    def default_active():
        return True
