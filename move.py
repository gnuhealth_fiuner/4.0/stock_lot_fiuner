from trytond.model import fields
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.pyson import Eval, Bool, Or


class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'

    only_available = fields.Boolean("Only available",
                states={'readonly': Or(
                                    Bool(Eval('lot')),
                                    Eval('state').in_(['cancelled', 'done']))
                })
    available = fields.Function(
            fields.Float("Available"),
            'on_change_with_available')
    lot_domain = fields.Function(
            fields.Many2Many('stock.lot', None, None, "Lot domain"),
            'on_change_with_lot_domain')

    @fields.depends('lot', 'from_location', 'to_location')
    def on_change_with_available(self, name=None):
        pool = Pool()
        Product = pool.get('product.product')
        Lot = pool.get('stock.lot')
        lot = self.lot
        product = lot and lot.product
        if self.from_location and self.to_location:
            if self.from_location.type in ['warehouse', 'storage']:
                location = self.from_location
            else:
                location = self.to_location

            if location.warehouse and location.id in [
                                        location.warehouse.input_location.id,
                                        location.warehouse.output_location.id,
                                        location.warehouse.storage_location.id]:
                location = location.warehouse.storage_location

            if lot and lot.product and location:
                location_id = location.id
                with Transaction().set_context(locations=[location_id]):
                    quantity = 0
                    if not lot:
                        quantity += product.quantity
                    else:
                        quantity = Lot.get_quantity([lot], 'quantity')
                        if lot.id in quantity:
                            quantity = quantity[lot.id]
                        else:
                            quantity = 0.0
                    return quantity
        return 0.0

    @fields.depends('only_available', 'product', 'from_location',
        'to_location')
    def on_change_with_lot_domain(self, name=None):
        pool = Pool()
        Lot = pool.get('stock.lot')
        lots = []
        location = None
        res = 0
        if self.from_location and self.to_location:
            if self.from_location.type in ['warehouse', 'storage']:
                location = self.from_location
            else:
                location = self.to_location

            if location.warehouse and location.id in [
                                        location.warehouse.input_location.id,
                                        location.warehouse.output_location.id,
                                        location.warehouse.storage_location.id]:
                location = location.warehouse.storage_location

            res = []
            if self.only_available and location and self.product:
                with Transaction().set_context(
                        locations=[location.id]):
                    lots =Lot.search([
                        ('id', '>', 0),
                        ('quantity', '>', 0),
                        ('product', '=', self.product.id)
                        ])
                    if lots:
                        res = [l.id for l in lots]
            elif self.product:
                lots = Lot.search([
                    ('product', '=', self.product.id)])
                if lots:
                    res = [l.id for l in lots]
        return res

    @fields.depends('lot')
    def on_change_with_only_available(self, name=None):
        if self.lot:
            return False
        return False

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.lot.domain.append(('id', 'in', Eval('lot_domain')))
