from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.pool import Pool

from datetime import date


class DeactivateLotSledStart(ModelView):
    "Deactivate Lot Sled - Start"
    __name__ = 'stock.lot.deactivate_sled.start'

    expiration_date = fields.Date('Expiration Date', required=True)

    @staticmethod
    def default_expiration_date():
        return date.today()


class DeactivateLotSledWizard(Wizard):
    "Deactivate Lot Sled - Wizard"
    __name__ = 'stock.lot.deactivate_sled.wizard'

    start = StateView('stock.lot.deactivate_sled.start',
                'stock_lot_fiuner.deactivate_lot_sled_start_form_view',[
                    Button('Deactivate', 'deactivate', 'tryton.ok', default=True),
                    Button('Cancel', 'end', 'tryton-cancel')
                    ])

    deactivate = StateTransition()

    def transition_deactivate(self):
        pool = Pool()
        Lot = pool.get('stock.lot')
        lots = Lot.search([
            ('expiration_date', '<', self.start.expiration_date)
            ])
        for lot in lots:
            lot.active = False
            lot.save()
        return 'end'
