from trytond.pool import Pool

from . import lot
from . import move

from . wizard import deactivate_lot_sled

def register():
    Pool.register(
        lot.Lot,
        move.Move,
        deactivate_lot_sled.DeactivateLotSledStart,
        module='stock_lot_fiuner', type_='model')
    Pool.register(
        deactivate_lot_sled.DeactivateLotSledWizard,
        module='stock_lot_fiuner', type_='wizard')
